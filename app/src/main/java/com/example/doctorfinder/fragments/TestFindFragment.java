package com.example.doctorfinder.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.R;
import com.example.doctorfinder.UserLocationActivity;
import com.example.doctorfinder.activitys.SearchActivity;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFindFragment extends Fragment implements View.OnClickListener {

    Button bloodTestBtn, liverTestBtn, xrayTestBtn, mriTestBtn, heartTestBtn, dentalTestBtn, earTestBtn, urineTestBtn, eyeTestBtn, diabetesTestBtn;

    TextView userLocationTv, searchViewClick;
    SharedPreferences sharedPreferences;
    String userLocation , longitudeValue , latetudeValue;
    FragmentTransaction fragmentTransaction;

    public TestFindFragment() {
        // Required empty public con
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_test_find, container, false);
        // Inflate the layout for this fragment

        bloodTestBtn=view.findViewById(R.id.test_btnBlood);
        liverTestBtn=view.findViewById(R.id.test_btnLiver);
        xrayTestBtn=view.findViewById(R.id.test_btnXray);
        mriTestBtn=view.findViewById(R.id.test_btnMri);
        heartTestBtn=view.findViewById(R.id.test_btnHeart);
        dentalTestBtn=view.findViewById(R.id.test_btnDental);
        earTestBtn=view.findViewById(R.id.test_btnEar);
        urineTestBtn=view.findViewById(R.id.test_btnUrine);
        eyeTestBtn=view.findViewById(R.id.test_btnEye);
        diabetesTestBtn=view.findViewById(R.id.test_btnDiabetes);
        userLocationTv=view.findViewById(R.id.TvLocation);
        searchViewClick=view.findViewById(R.id.tvFragmentTestFind);
        //signupBtn=view.findViewById(R.id.btnSignUp);

        bloodTestBtn.setOnClickListener(this);
        liverTestBtn.setOnClickListener(this);
        xrayTestBtn.setOnClickListener(this);
        mriTestBtn.setOnClickListener(this);
        heartTestBtn.setOnClickListener(this);
        dentalTestBtn.setOnClickListener(this);
        earTestBtn.setOnClickListener(this);
        urineTestBtn.setOnClickListener(this);
        eyeTestBtn.setOnClickListener(this);
        diabetesTestBtn.setOnClickListener(this);
        searchViewClick.setOnClickListener(this);
        //signupBtn.setOnClickListener(this);

        sharedPreferences = (Objects.requireNonNull(getActivity())).getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        userLocation = sharedPreferences.getString("userLocationKey", "No location found");
        longitudeValue = sharedPreferences.getString("longitudeKey", "23.750894508789");
        latetudeValue = sharedPreferences.getString("latitudeKey", "90.3674087524414");


        userLocationTv.setText(userLocation);

        userLocationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putBoolean("isSetLocation",false);

                editor.apply();
                editor.commit();

                Intent intent = new Intent(getActivity(), UserLocationActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
        {

            switch (v.getId()) {
                case R.id.test_btnBlood:
                    Toast.makeText(getActivity(), "Blood test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnLiver:
                    Toast.makeText(getActivity(), "Liver test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnXray:
                    Toast.makeText(getActivity(), "X-ray test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnMri:
                    Toast.makeText(getActivity(), "MRI test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnHeart:
                    Toast.makeText(getActivity(), "Heart test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnDental:
                    Toast.makeText(getActivity(), "Dental test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnEar:
                    Toast.makeText(getActivity(), "Ear test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnUrine:
                    Toast.makeText(getActivity(), "Urine test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnEye:
                    Toast.makeText(getActivity(), "Eye test clicked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.test_btnDiabetes:
                    Toast.makeText(getActivity(), "Diabetes test clicked", Toast.LENGTH_SHORT).show();
                    break;

                /*case R.id.btnSearch:
                    Intent intent=new Intent(getActivity(), SearchActivity.class);
                    startActivity(intent);
                    break;*/
                 /*case R.id.btnSignUp:
                     Intent i =  new Intent(getActivity(), SignUpActivity.class);
                     startActivity(i);
                     break;*/

                case R.id.tvFragmentTestFind:
                    Intent intent=new Intent(getActivity(), SearchActivity.class);
                    startActivity(intent);
                    break;

            }
        }
    }
}
