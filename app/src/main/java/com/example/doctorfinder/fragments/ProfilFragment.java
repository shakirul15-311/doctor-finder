package com.example.doctorfinder.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.doctorfinder.R;
import com.example.doctorfinder.activitys.PatientProfileActivity;
import com.example.doctorfinder.activitys.SignInActivity;
import com.example.doctorfinder.activitys.SignUpActivity;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilFragment extends Fragment implements View.OnClickListener{


   TextView signinUpdatepatientProfile, patientName;
   ImageView patientProfilePhoto;

   public ProfilFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_profil, container, false);

        signinUpdatepatientProfile = view.findViewById(R.id.signinUpdatePatientProfile);
        patientProfilePhoto = view.findViewById(R.id.ivPatientImage);
        patientName = view.findViewById(R.id.tvPatientName);

        //Picasso.get().load("https://i.dailymail.co.uk/i/newpix/2018/06/04/11/0531F97F000007D0-0-image-a-4_1528108270693.jpg").into(patientProfilePhoto);

        signinUpdatepatientProfile.setOnClickListener(this);
        patientProfilePhoto.setOnClickListener(this);
        patientName.setOnClickListener(this);

        return view;


    }

    @Override
    public void onClick(View v) {

        signinUpdatepatientProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPatientProfile = new Intent(getActivity(),SignInActivity.class);
                startActivity(intentPatientProfile);
            }
        });


        patientProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPatientProfile = new Intent(getActivity(),PatientProfileActivity.class);
                startActivity(intentPatientProfile);
            }
        });


        patientName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPatientProfile = new Intent(getActivity(),PatientProfileActivity.class);
                startActivity(intentPatientProfile);
            }
        });




    }
}
