package com.example.doctorfinder.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.DoctorMapsActivity;
import com.example.doctorfinder.R;
import com.example.doctorfinder.UserLocationActivity;
import com.example.doctorfinder.activitys.SearchActivity;
import com.example.doctorfinder.activitys.SetLocationActivity;

import java.util.Objects;



public class DoctorFindFragment extends Fragment  implements View.OnClickListener {
    Button heartBtn, generlDoctorBtn, childSpecilistBtn, womenSpecilostBtn, skinBtn, dentalBtn,
                     earNoseBtn, boneJoinBtn, diabeticsBtn, cancerBtn, kidneyBtn,surgeryBtn,brainBtn,
                     lungsBtn, mentalWellnessBtn, digestiveBtn, psychologistBtn, physiothereapist,
                     homeopathyBtn, ayurvedaBtn, eyeSpecilistBtn, sexSpecilistBtn, searchBtn, signupBtn;

    TextView userLocationTv, searchViewClick;
    SharedPreferences sharedPreferences;
    String userLocation , longitudeValue , latetudeValue;
    FragmentTransaction fragmentTransaction;



    public DoctorFindFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_doctor_find, container, false);
        // Inflate the layout for this fragment

        heartBtn=view.findViewById(R.id.btnHeart);
        generlDoctorBtn=view.findViewById(R.id.btnGeneralDoctor);
        childSpecilistBtn=view.findViewById(R.id.btnChildSpecialist);
        womenSpecilostBtn=view.findViewById(R.id.btnWomenHealth);
        skinBtn=view.findViewById(R.id.btnSkinHealth);
        dentalBtn=view.findViewById(R.id.btnDentalCare);
        earNoseBtn=view.findViewById(R.id.btnEarNose);
        boneJoinBtn=view.findViewById(R.id.btnBoneJoint);
        diabeticsBtn=view.findViewById(R.id.btnDiabetes);
        cancerBtn=view.findViewById(R.id.btnCancer);
        kidneyBtn=view.findViewById(R.id.btnKidney);
        surgeryBtn=view.findViewById(R.id.btnSurgery);
        brainBtn=view.findViewById(R.id.btnBrain);
        lungsBtn=view.findViewById(R.id.btnLungs);
        mentalWellnessBtn=view.findViewById(R.id.btnMentalWellness);
        digestiveBtn=view.findViewById(R.id.btnDigestiveIssue);
        psychologistBtn=view.findViewById(R.id.btnPsychologist);
        physiothereapist=view.findViewById(R.id.btnPhysiotherapist);
        homeopathyBtn=view.findViewById(R.id.btnHomeopathy);
        ayurvedaBtn=view.findViewById(R.id.btnAyurveda);
        eyeSpecilistBtn=view.findViewById(R.id.btnEyeSpecialist);
        sexSpecilistBtn=view.findViewById(R.id.btnSexSpecialist);
        //searchBtn=view.findViewById(R.id.btnSearch);
        searchViewClick=view.findViewById(R.id.tvFragmentDoctorFind);
        userLocationTv=view.findViewById(R.id.TvLocation);

        heartBtn.setOnClickListener(this);
        generlDoctorBtn.setOnClickListener(this);
        childSpecilistBtn.setOnClickListener(this);
        womenSpecilostBtn.setOnClickListener(this);
        skinBtn.setOnClickListener(this);
        dentalBtn.setOnClickListener(this);
        earNoseBtn.setOnClickListener(this);
        eyeSpecilistBtn.setOnClickListener(this);
        sexSpecilistBtn.setOnClickListener(this);
        boneJoinBtn.setOnClickListener(this);
        diabeticsBtn.setOnClickListener(this);
        cancerBtn.setOnClickListener(this);
        kidneyBtn.setOnClickListener(this);
        surgeryBtn.setOnClickListener(this);
        brainBtn.setOnClickListener(this);
        lungsBtn.setOnClickListener(this);
        mentalWellnessBtn.setOnClickListener(this);
        digestiveBtn.setOnClickListener(this);
        psychologistBtn.setOnClickListener(this);
        physiothereapist.setOnClickListener(this);
        homeopathyBtn.setOnClickListener(this);
        ayurvedaBtn.setOnClickListener(this);
        //searchBtn.setOnClickListener(this);
        searchViewClick.setOnClickListener(this);


        fragmentTransaction = getFragmentManager().beginTransaction();
        sharedPreferences = (Objects.requireNonNull(getActivity())).getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        userLocation = sharedPreferences.getString("userLocationKey", "No location found");
        longitudeValue = sharedPreferences.getString("longitudeKey", "23.750894508789");
        latetudeValue = sharedPreferences.getString("latitudeKey", "90.3674087524414");


        userLocationTv.setText(userLocation);

        userLocationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putBoolean("isSetLocation",false);

                editor.apply();
                editor.commit();

                Intent intent = new Intent(getActivity(), UserLocationActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
         {

             switch (v.getId()) {
                 case R.id.btnHeart:
                    Intent i=new Intent(getActivity(), DoctorMapsActivity.class);
                     i.putExtra("categoryBtnId","heart");
                     startActivity(i);
                     break;

                 case R.id.btnGeneralDoctor:
                     Intent intentGeneralDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentGeneralDoctor.putExtra("categoryBtnId","general");
                     startActivity(intentGeneralDoctor);
                     break;

                 case R.id.btnChildSpecialist:
                     Intent intentChildDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentChildDoctor.putExtra("categoryBtnId","child");
                     startActivity(intentChildDoctor);
                     break;
                 case R.id.btnWomenHealth:
                     Intent intentWomendDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentWomendDoctor.putExtra("categoryBtnId","women");
                     startActivity(intentWomendDoctor);
                     break;
                 case R.id.btnSkinHealth:
                     /*Intent intentSkinndDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentSkinndDoctor.putExtra("categoryBtnId","skin");
                     startActivity(intentSkinndDoctor);*/
                     Intent intentSkinndDoctor=new Intent(getActivity(), UserLocationActivity.class);
                     //intentSkinndDoctor.putExtra("categoryBtnId","women");
                     startActivity(intentSkinndDoctor);
                     break;
                 case R.id.btnDentalCare:
                     Intent intentDentalDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentDentalDoctor.putExtra("categoryBtnId","dental");
                     startActivity(intentDentalDoctor);
                     break;
                 case R.id.btnEarNose:
                     Intent intentEarNoseDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentEarNoseDoctor.putExtra("categoryBtnId","earnose");
                     startActivity(intentEarNoseDoctor);
                     break;
                 case R.id.btnEyeSpecialist:
                     Intent intentEyeDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentEyeDoctor.putExtra("categoryBtnId","eye");
                     startActivity(intentEyeDoctor);
                     break;
                 case R.id.btnSexSpecialist:
                     Intent intentSexDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentSexDoctor.putExtra("categoryBtnId","sex");
                     startActivity(intentSexDoctor);
                     break;
                 case R.id.btnBoneJoint:
                     Intent intentBoneDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentBoneDoctor.putExtra("categoryBtnId","bone");
                     startActivity(intentBoneDoctor);
                     break;
                 case R.id.btnDiabetes:
                     Intent intentDiabetesDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentDiabetesDoctor.putExtra("categoryBtnId","diabetes");
                     startActivity(intentDiabetesDoctor);
                     break;
                 case R.id.btnCancer:
                     Intent intentCancerdDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentCancerdDoctor.putExtra("categoryBtnId","cancer");
                     startActivity(intentCancerdDoctor);
                     break;
                 case R.id.btnKidney:
                     Intent intentKidneyDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentKidneyDoctor.putExtra("categoryBtnId","kidney");
                     startActivity(intentKidneyDoctor);
                     break;
                 case R.id.btnSurgery:
                     Intent intentSurgeryDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentSurgeryDoctor.putExtra("categoryBtnId","surgery");
                     startActivity(intentSurgeryDoctor);
                     break;
                 case R.id.btnBrain:
                     Intent intentBrainDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentBrainDoctor.putExtra("categoryBtnId","brain");
                     startActivity(intentBrainDoctor);
                     break;
                 case R.id.btnLungs:
                     Intent intentLungsDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentLungsDoctor.putExtra("categoryBtnId","lungs");
                     startActivity(intentLungsDoctor);
                     break;
                 case R.id.btnMentalWellness:
                     Intent intentMentalWellnessdDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentMentalWellnessdDoctor.putExtra("categoryBtnId","mentawellness");
                     startActivity(intentMentalWellnessdDoctor);
                     break;
                 case R.id.btnDigestiveIssue:
                     Intent intentDigestiveDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentDigestiveDoctor.putExtra("categoryBtnId","digestive");
                     startActivity(intentDigestiveDoctor);
                     break;
                 case R.id.btnPsychologist:
                     Intent intentPsychologistDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentPsychologistDoctor.putExtra("categoryBtnId","phycologist");
                     startActivity(intentPsychologistDoctor);
                     break;
                 case R.id.btnPhysiotherapist:
                     Intent intentPhysiotherapistDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentPhysiotherapistDoctor.putExtra("categoryBtnId","physiotherapistd");
                     startActivity(intentPhysiotherapistDoctor);
                     break;
                 case R.id.btnHomeopathy:
                     Toast.makeText(getActivity(), "Button Homeopathy clicked", Toast.LENGTH_SHORT).show();
                     Intent intentHomeopathydDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentHomeopathydDoctor.putExtra("categoryBtnId","homeopathy");
                     startActivity(intentHomeopathydDoctor);
                     break;
                 case R.id.btnAyurveda:
                     Intent intentAyurvedaDoctor=new Intent(getActivity(), DoctorMapsActivity.class);
                     intentAyurvedaDoctor.putExtra("categoryBtnId","ayurveda");
                     startActivity(intentAyurvedaDoctor);
                     break;

                 case R.id.tvFragmentDoctorFind:
                     Intent intent=new Intent(getActivity(), SearchActivity.class);
                     startActivity(intent);
                     break;

             }
        }
    }
}
