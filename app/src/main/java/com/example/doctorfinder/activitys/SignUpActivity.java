package com.example.doctorfinder.activitys;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.SigningInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.doctorfinder.R;

import java.util.Calendar;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText patientName, patientEmail, patienGender, patientPassword, patientBirthDate, patientBloodGroup,patientPhone ;

    private Button patientSignupBtn;
    private TextView patientSignuptoSignin;
    private String[] bloodGroupList = {"A+","A-","AB+","AB-","B+","B-","O+","O-"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

       patientSignupBtn = (Button) findViewById(R.id.patient_signup_btn);
       patientSignuptoSignin = (TextView) findViewById(R.id.patient_signup_to_signin);

       patientName = findViewById(R.id.etv_patient_name);
       patientEmail= findViewById(R.id.etv_patient_email);
       patientPassword = findViewById(R.id.etv_patient_password);
       patientBloodGroup = findViewById(R.id.etv_patient_bloodGroup);
       patientBirthDate = findViewById(R.id.etv_patient_dateofbirth);
       patienGender = findViewById(R.id.etv_patient_gender);
       patientPhone = findViewById(R.id.etv_patient_phone);


        patientSignuptoSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }
//--------


    public void openSignInActivity(){
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.patient_signup_btn:
                patientSignUp();
                break;
            case R.id.patient_signup_to_signin:

                break;

        }
    }


    private void patientSignUp() {
        String email= patientEmail.getText().toString().trim();
        String name= patientName.getText().toString().trim();
        String password= patientPassword.getText().toString().trim();
        String bloodgroup= patientBloodGroup.getText().toString().trim();
        String dateofbirth= patientBirthDate.getText().toString().trim();
        String phone= patientPhone.getText().toString().trim();
        String gender= patienGender.getText().toString().trim();

        if (email.isEmpty()) {
            patientEmail.setError("Email is required");
            patientEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            patientEmail.setError("Enter a valid email");
            patientEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            patientPassword.setError("Password required");
            patientPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            patientPassword.setError("Password should be atleast 6 character long");
            patientPassword.requestFocus();
            return;
        }

        if (name.isEmpty()) {
            patientName.setError("Name required");
            patientName.requestFocus();
            return;
        }

        if (bloodgroup.isEmpty()) {
            patientBloodGroup.setError("Blood group required");
            patientBloodGroup.requestFocus();
            return;
        }

        if (dateofbirth.isEmpty()) {
            patientBirthDate.setError("Date of birth required");
            patientBirthDate.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            patientPhone.setError("Phone number required");
            patientPhone.requestFocus();
            return;
        }

        if (gender.isEmpty()) {
            patienGender.setError("Gender required");
            patienGender.requestFocus();
            return;
        }

        /*Api all for signup*/

    }
}