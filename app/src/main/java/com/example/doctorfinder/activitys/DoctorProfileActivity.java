package com.example.doctorfinder.activitys;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.R;
import com.example.doctorfinder.models.DoctorInfo;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

public class DoctorProfileActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    TextView doctorNameTv, doctorDegreeTv, doctorContactTv,
            doctorSpecialityTv, docProfileBackBtn;
    ImageView doctorImageIv;
    DoctorInfo doctorInfo;
    Button doctorCallBtn;
    private static final int REQUEST_CALL = 1;
    String doctorCall;

    double latitude;
    double longitude;

    private TextView locationtextView;
    Geocoder geocoder;
    List<Address> doctorAddresses;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);

        ImageView im = findViewById(R.id.ivRvDoctor);



//find id----
        doctorNameTv = findViewById(R.id.tvDoctorName);
        doctorDegreeTv = findViewById(R.id.tvDoctorDegree);
        doctorSpecialityTv = findViewById(R.id.tvDoctorSpeciality);
        doctorImageIv = findViewById(R.id.ivDoctorImage);
        doctorCallBtn = findViewById(R.id.btnCallDoctor);
        docProfileBackBtn = findViewById(R.id.tvDocProfileBackBtn);
        locationtextView = findViewById(R.id.doctorLocationMap);


//Set Text-----------
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String doctorName = bundle.getString("doctorName");
            doctorNameTv.setText(doctorName);

            String doctorDegree = bundle.getString("doctorDegree");
            doctorDegreeTv.setText(doctorDegree);

            String doctorSpeciality = bundle.getString("doctorSpeciality");
            doctorSpecialityTv.setText(doctorSpeciality);

            //String doctorImage = getIntent().getStringExtra("doctorImage");
            String doctorImage = bundle.getString("doctorImage");
            //String doctorImage1="https://ak7.picdn.net/shutterstock/videos/5790257/thumb/11.jpg";
            Picasso.get().load(doctorImage).into(doctorImageIv);

            doctorCall = bundle.getString("doctorCall");

            latitude = getIntent().getDoubleExtra("doctorLocationLatitude", 0);
            longitude = getIntent().getDoubleExtra("doctorLocationLongitude", 0);
            //Toast.makeText(this, String.valueOf(latitude)+" "+String.valueOf(longitude), Toast.LENGTH_SHORT).show();

            //Location
            geocoder = new Geocoder(this);
            try {
                doctorAddresses = geocoder.getFromLocation(longitude, latitude,1);
                /*for (int i =0; i<doctorAddresses.size(); i++){*/
                    String address = doctorAddresses.get(0).getAddressLine(0);
                    String area = doctorAddresses.get(0).getLocality();
                    String city = doctorAddresses.get(0).getAdminArea();
                    String country = doctorAddresses.get(0).getCountryName();
                    String postalCode = doctorAddresses.get(0).getPostalCode();


                String fulladdress= address;
                    locationtextView.setText(fulladdress);
                /*}*/

            } catch (IOException e) {
                e.printStackTrace();
            }


        }


        //Back btn
        docProfileBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                return;
            }
        });


    }



//Call
    public void call(View view) {
        String phoneNumber = String.valueOf(doctorCall);
        String dial = "tel:" + phoneNumber.trim();

        if (ActivityCompat.checkSelfPermission(DoctorProfileActivity.this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DoctorProfileActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);

        }else {
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }

        }
    }



    public void doctorLocationMap(View view) {
    }
}

