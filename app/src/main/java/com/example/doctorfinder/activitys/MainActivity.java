package com.example.doctorfinder.activitys;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.view.MenuItem;
import android.view.View;

import com.example.doctorfinder.R;
import com.example.doctorfinder.fragments.DoctorFindFragment;
import com.example.doctorfinder.fragments.ProfilFragment;
import com.example.doctorfinder.fragments.TestFindFragment;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Fade fade = new Fade();
        View decor = getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);

        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);*/

        bottomNadView = findViewById(R.id.bottomNavigation);
        bottomNadView.setOnNavigationItemSelectedListener(navListener);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
                    new DoctorFindFragment()).commit();
        }
    }

     BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.navDoctorFind:
                            selectedFragment = new DoctorFindFragment();
                            break;
                        case R.id.navTestFind:
                            selectedFragment = new TestFindFragment();
                            break;
                        case R.id.navUserProfile:
                            selectedFragment = new ProfilFragment();
                            break;

                    }

                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
                            selectedFragment).commit();

                    return true;
                }
            };

}
