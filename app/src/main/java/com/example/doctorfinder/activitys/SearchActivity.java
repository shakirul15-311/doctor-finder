package com.example.doctorfinder.activitys;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.R;
import com.example.doctorfinder.adapters.DoctorListAdapter;
import com.example.doctorfinder.api.Api;
import com.example.doctorfinder.api.ClientApi;
import com.example.doctorfinder.models.DoctorInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<DoctorInfo> doctorInfoData;
    private DoctorListAdapter adapter;
    private Api api;
    SearchView doctorSearchSv;
    String url;
    ProgressBar progressBar;

    SharedPreferences sharedPreferences;

    private TextView svBackButton, errorMwssageTv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        doctorSearchSv= findViewById(R.id.svSearchlocation);
        errorMwssageTv= findViewById(R.id.tVErrorMessage);
        progressBar= findViewById(R.id.progress_bar);
        svBackButton= findViewById(R.id.svBackBtn);


        doctorInfoData = new ArrayList<>();
        recyclerView = findViewById(R.id.doctorListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sharedPreferences = this.getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        final String longitudeValue = sharedPreferences.getString("longitudeKey", "23.7805733");
        final String latitudeValue = sharedPreferences.getString("latitudeKey", "90.2792399");


        /*Retrofit retrofit= new Retrofit.Builder()
                .baseUrl("http://robiulhasan-001-site1.gtempurl.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/

        api = ClientApi.getmInstance().getApi();

        assert longitudeValue != null;
        assert latitudeValue != null;
        double lat =Double.valueOf(latitudeValue);
        double lng =Double.valueOf(longitudeValue);

        getNearByDoctor(lat,lng);

    }

    @SuppressLint({"ClickableViewAccessibility", "DefaultLocale"})
    private void getNearByDoctor(final  double longitudeValue, final double latitudeValue) {

        url = String.format("locations?longitude=%f&latitude=%f", longitudeValue, latitudeValue);
        //url="locations?longitude=26.56532365&latitude=90.84764516&category=3";

        Call<List<DoctorInfo>> call = api.getDoctorinfo(url);
        call.enqueue(new Callback<List<DoctorInfo>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<DoctorInfo>> call, Response<List<DoctorInfo>> response) {
                if (response.isSuccessful()){
                    doctorInfoData = response.body();
                    adapter = new DoctorListAdapter(SearchActivity.this, doctorInfoData);
                    recyclerView.setAdapter(adapter);

                    //Toast.makeText(SearchActivity.this, "success "+response.code(), Toast.LENGTH_SHORT).show();

                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                    errorMwssageTv.setText("Error 1:"+response.code());
                    //Toast.makeText(SearchActivity.this, "error "+response.code(), Toast.LENGTH_SHORT).show();
                }

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFailure(Call<List<DoctorInfo>> call, Throwable t) {
                errorMwssageTv.setText("Network error");
                doctorSearchSv.setVisibility(View.INVISIBLE);
                doctorSearchSv.setBackgroundColor(getResources().getColor(R.color.colorLightLightblue));
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        doctorSearchSv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        doctorSearchSv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        svBackButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                onBackPressed();
                return false;
            }
        });
    }

}