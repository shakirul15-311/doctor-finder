package com.example.doctorfinder.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.doctorfinder.R;
import com.example.doctorfinder.api.ClientApi;
import com.example.doctorfinder.models.LoginResponse;
import com.example.doctorfinder.retrofit.RetrofitInstance;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button,PatientLoginButton;
    private TextView SigninSignup;
    private EditText userLoginEmail, userLoginPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        userLoginEmail=findViewById(R.id.etv_patient_email);
        userLoginPassword=findViewById(R.id.etv_patient_password);

        findViewById(R.id.btnPatientLogin).setOnClickListener(this);
        findViewById(R.id.tvSignin_to_singup).setOnClickListener(this);


        SigninSignup = (TextView) findViewById(R.id.tvSignin_to_singup);
        SigninSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignUpActivity();

            }
        });

    }


    //

    public void openSignUpActivity(){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnPatientLogin:
                UserLogin();
                break;
            case  R.id.etv_patient_password:
                break;
        }
    }


    private void UserLogin() {


        String username = userLoginEmail.getText().toString().trim();
        String password = userLoginPassword.getText().toString().trim();

        if (username.isEmpty()) {
            userLoginEmail.setError("Email is required");
            userLoginEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            userLoginEmail.setError("Enter a valid email");
            userLoginEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            userLoginPassword.setError("Password required");
            userLoginPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            userLoginPassword.setError("Password should be atleast 6 character long");
            userLoginPassword.requestFocus();
            return;
        }

    }
}
