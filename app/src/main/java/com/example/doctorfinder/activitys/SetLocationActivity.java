package com.example.doctorfinder.activitys;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;


public class SetLocationActivity extends AppCompatActivity{

    AutoCompleteTextView userLocationAc;
    Button setlocationBtn, currentLocationbtn;
    TextView textView, tvViewLocalyLocation;

    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;
    Geocoder geocoder;
    List<Address> addresses;

    String longitude, latitude;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_location);
        geocoder = new Geocoder(this);

        userLocationAc=findViewById(R.id.acUserLocation);
        setlocationBtn=findViewById(R.id.btnSetLocation);
        currentLocationbtn=findViewById(R.id.btnCurrentLocationSet);
        textView =findViewById(R.id.tvViewLocation);
        tvViewLocalyLocation =findViewById(R.id.tvViewLocalyLocation);

        sharedPreferences = getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("isSetLocation",false)){
            Intent intent=new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    /*textView.setText(String.valueOf(location.getLatitude()));
                    textView.setText(String.valueOf(location.getLongitude()));*/
                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
                        textView.setText(addresses.get(0).getAddressLine(0)+"\n"
                                /*+addresses.get(0).getLocality()+"\n"+
                                addresses.get(0).getCountryName()*/);
                        userLocationAc.setText(addresses.get(0).getLocality());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        currentLocationbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLastLocation();
                requestCurrentLocationUpdate();
            }
        });

        setlocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userLocation = userLocationAc.getText().toString().trim();
                if (userLocation.equals("")){
                    Toast.makeText(SetLocationActivity.this, "Set location", Toast.LENGTH_SHORT).show();
                }else {
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putBoolean("isSetLocation",true);
                    editor.putString("userLocationKey",userLocation);
                    editor.putString("longitudeKey",longitude);
                    editor.putString("latitudeKey",latitude);
                    editor.apply();
                    editor.commit();
                    Intent intent=new Intent(SetLocationActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    private void requestCurrentLocationUpdate() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},2);
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    public void getUserLastLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1);
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null){
                    latitude=(String.valueOf(location.getLatitude()));
                    longitude=(String.valueOf(location.getLongitude()));
                }
            }
        });
    }
}
