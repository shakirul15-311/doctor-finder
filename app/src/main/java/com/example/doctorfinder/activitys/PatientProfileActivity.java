package com.example.doctorfinder.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.doctorfinder.R;

public class PatientProfileActivity extends AppCompatActivity {

    TextView patientProfileBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_profile);


        patientProfileBackBtn=findViewById(R.id.tvPatientProfileBackBtn);



        patientProfileBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
