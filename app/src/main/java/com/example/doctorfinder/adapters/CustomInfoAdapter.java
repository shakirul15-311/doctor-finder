package com.example.doctorfinder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.doctorfinder.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import org.w3c.dom.Text;

public class CustomInfoAdapter  implements GoogleMap.InfoWindowAdapter{
    private final View mWindow;
    private Context mContext;
    private TextView doctorDegree;

    public CustomInfoAdapter(Context context) {
        mContext = context;
        mWindow = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null);
    }

    private void rendowWindowText(Marker marker, View view){

        String doctorName = marker.getTitle();
        TextView tvName = view.findViewById(R.id.tvDoctorNameInfoWnidow);
        if(!doctorName.equals("")){
            tvName.setText(doctorName);
        }

        String doctorDegree = marker.getId();
        TextView tvDegree =  view.findViewById(R.id.tvDoctorDegreeInfoWnidow);
        if(!doctorDegree.equals("")){
            tvDegree.setText(doctorDegree);
        }

        String doctorlocation = marker.getSnippet();
        TextView tvDoctorlocation =  view.findViewById(R.id.tvDoctorLocationInfoWnidow);
        if(!doctorlocation.equals("")){
            tvDoctorlocation.setText(doctorlocation);
        }



    }




    @Override
    public View getInfoWindow(Marker marker) {
        rendowWindowText(marker, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        rendowWindowText(marker, mWindow);
        return mWindow;
    }
}
