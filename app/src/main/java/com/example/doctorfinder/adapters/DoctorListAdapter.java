package com.example.doctorfinder.adapters;

import android.content.Context;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.R;
import com.example.doctorfinder.activitys.DoctorProfileActivity;
import com.example.doctorfinder.models.DoctorInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.MyViewHolder> implements Filterable {
    public Context context, context1;
    private List<DoctorInfo> doctorInfoList;
    private List<DoctorInfo> doctorInfoListFull;
    TextView errorMessageTv;
    ProgressBar progressBar;
    TextView locationtextView;


    public DoctorListAdapter(Context context, List<DoctorInfo>doctorInfoList){
        this.doctorInfoList =doctorInfoList;
        this.context=context;
        doctorInfoListFull = new ArrayList<>(this.doctorInfoList);


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doctor_info,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        errorMessageTv = view.findViewById(R.id.tVErrorMessage);
        progressBar = view.findViewById(R.id.progress_bar);
        locationtextView = view.findViewById(R.id.doctorLocationMap);
        return holder;
    }

    //data set
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final DoctorInfo getItemId = doctorInfoList.get(position);
        final DoctorInfo doctorInfoData= doctorInfoList.get(position);
        //Put Doctor details by id
        holder.doctorNameTv.setText(doctorInfoData.getName());
        holder.specialityTv.setText(doctorInfoData.getSpecialist());

        // Load Images
        //Picasso.get().load("https://ak7.picdn.net/shutterstock/videos/5790257/thumb/11.jpg").into(holder.im);
        Picasso.get().load(doctorInfoData.getImage()).into(holder.im);
        //Toast.makeText(context, doctorInfoData.getImage(), Toast.LENGTH_SHORT).show();

        holder.doctorDetailsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DoctorProfileActivity.class);
                intent.putExtra("doctorName", getItemId.getName());

                intent.putExtra("doctorDegree", getItemId.getDegrees());
                intent.putExtra("doctorSpeciality", getItemId.getSpecialist());
                intent.putExtra("doctorCall", getItemId.getContact());

                //intent.putExtras("doctorRating", getItemId.getDoctorRatings());


                intent.putExtra("doctorImage",getItemId.getImage());
                //Picasso.get().load(getItemId.getImage()).into(holder.im2);
                //Picasso.get().load("https://ak5.picdn.net/shutterstock/videos/25977905/thumb/1.jpg").into(holder.im2);

                intent.putExtra("doctorLocationLatitude", getItemId.getLatitude());
                intent.putExtra("doctorLocationLongitude", getItemId.getLongitude());


                context.startActivity(intent);

            }
        });

    }



    @Override
    public int getItemCount() {
        return doctorInfoList.size();
    }

    @Override
    public Filter getFilter() {
        return doctorIifoFilter;
    }

    private Filter doctorIifoFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DoctorInfo> filteredDoctorList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredDoctorList.addAll(doctorInfoListFull);
            } else {
                String filterPattarn = constraint.toString().toLowerCase().trim();
                for (DoctorInfo infoItem : doctorInfoListFull) {
                    if (infoItem.getName().toLowerCase().trim().contains(filterPattarn)) {
                        filteredDoctorList.add(infoItem);
                    }
                    if (infoItem.getSpecialist().toLowerCase().trim().contains(filterPattarn)) {
                        filteredDoctorList.add(infoItem);
                    }
                }
                if (filteredDoctorList.isEmpty()){
                    //errorMessageTv.setText("Doctor not found");
                    //progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(context, "Doctor not found", Toast.LENGTH_SHORT).show();
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredDoctorList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            doctorInfoList.clear();
            doctorInfoList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    // View Holder

    public static  class MyViewHolder extends RecyclerView.ViewHolder {

        TextView doctorNameTv,specialityTv;
        CardView doctorDetailsCardView;
        /*TextView doctorCall1;*/

        Button doctorCall;
        ImageView im,im2;
        String url;

        public MyViewHolder(@NonNull final View view) {
            super(view);

            doctorNameTv= view.findViewById(R.id.tvRvDoctorName);

            im = (ImageView) view.findViewById(R.id.ivRvDoctor);
            im2= (ImageView) view.findViewById(R.id.ivDoctorImage);

            url = "https://image.shutterstock.com/image-photo/attractive-female-doctor-front-medical-260nw-557085679.jpg";

            specialityTv=view.findViewById(R.id.tvRvDoctorSpeciality);
            doctorDetailsCardView=view.findViewById(R.id.doctorDetailsCardViewId);
        }
    }
}