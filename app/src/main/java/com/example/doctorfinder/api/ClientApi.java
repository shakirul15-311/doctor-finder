package com.example.doctorfinder.api;

import android.util.Base64;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientApi {

    private static final String AUTH = "Basic " + Base64.encodeToString(("Robiul:12345").getBytes(), Base64.NO_WRAP);
    private static final String BASE_URL="http://robiulhasan-001-site1.gtempurl.com/";
    private static ClientApi mInstance;
    private Retrofit retrofit;

    private ClientApi(){
        retrofit= new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }




    public static synchronized ClientApi getmInstance(){
        if(mInstance == null){
            mInstance = new ClientApi();
        }
        return mInstance;
    }

    public Api getApi(){
        return  retrofit.create(Api.class);
    }
}

