
package com.example.doctorfinder.api;

import com.example.doctorfinder.models.DocterHeartInfo;
import com.example.doctorfinder.models.DoctorInfo;
import com.example.doctorfinder.models.LoginResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {

    /*@GET("locations")
    Call<List<DoctorInfo>> getDoctorinfo(@Query("longitude") Double longitudeKey,
                                         @Query("latitude") Double latitudeKey
                                         );*/

    @GET()
    Call<List<DoctorInfo>> getDoctorinfo (@Url String url);

    @GET()
    Call<List<DocterHeartInfo>> getDocterHeartInfo (@Url String url);

    @GET("locations?longitude={lng}&latitude=90.84764516")
    Call<List<DoctorInfo>> getDocterInfoDemo ();





    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> userLogin(
            @Field("username") String username,
            @Field("password") String password
    );



}