package com.example.doctorfinder.models;

public class PatientInfo {

    private int id;
    private String name, contact, bloodGroup, gender, dateOfBirth;

    public PatientInfo(int id, String name, String contact, String bloodGroup, String gender, String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.bloodGroup = bloodGroup;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getGender() {
        return gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
}
