package com.example.doctorfinder.models;

public class LoginResponse {

    private boolean error;
    private String message;
    private PatientInfo user;

    public LoginResponse(boolean error, String message, PatientInfo user) {
        this.error = error;
        this.message = message;
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public PatientInfo getUser() {
        return user;
    }
}
