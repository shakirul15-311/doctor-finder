package com.example.doctorfinder;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.activitys.DoctorProfileActivity;
import com.example.doctorfinder.api.Api;
import com.example.doctorfinder.api.ClientApi;
import com.example.doctorfinder.models.DocterHeartInfo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    SharedPreferences sharedPreferences;
    private Api api;
    String url, doctorName, doctorDegree;


    Double latitudeApiValue, longitudeApiValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sharedPreferences = this.getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        final String longitudeValue = sharedPreferences.getString("longitudeKey", "23.7805733");
        final String latitudeValue = sharedPreferences.getString("latitudeKey", "90.2792399");

        assert longitudeValue != null;
        assert latitudeValue != null;
        double lat = Double.valueOf(latitudeValue);
        double lng = Double.valueOf(longitudeValue);

        api = ClientApi.getmInstance().getApi();


        // -- Doctor fragment cards to map
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String doctorCategoryId;
            doctorCategoryId = bundle.getString("categoryBtnId");


            if (doctorCategoryId.equals("heart")){
                getHeartCategoryDoctor(lat, lng);
            }
            else if (doctorCategoryId.equals("general")){
                getGeneralCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("child")){
                getChildCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("women")){
                getWomenCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("skin")){
                getSkinCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("dental")){
                getDentalCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("earnose")){
                getEarNoseCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("eye")){
                getEyeCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("sex")){
                getSexCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("bone")){
                getBoneCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("diabetes")){
                getDiabetesCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("cancer")){
                getCancerCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("kidney")){
                getKidneyCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("surgery")){
                getSurgeryCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("brain")){
                getBrainCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("lungs")){
                getLungsCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("mentawellness")){
                getMentawellnessCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("digestive")){
                getDigestiveCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("phycologist")){
                getPhycologistCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("physiotherapistd")){
                getPhysiotherapistdCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("homeopathy")){
                getHomeopathyCategoryDoctor(lat, lng);
            }
            else if(doctorCategoryId.equals("ayurveda")){
                getAyurvedaCategoryDoctor(lat, lng);
            }


            //Toast.makeText(this, ""+doctorCategoryId, Toast.LENGTH_SHORT).show();

        }
    }


    //Heart Catagory 1
    private void getHeartCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=1", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //General Catagory 2
    private void getGeneralCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=2", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Child Catagory 3
    private void getChildCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=3", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Women Catagory 4
    private void getWomenCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=4", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Skin Catagory 5
    private void getSkinCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=5", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Dental Catagory 6
    private void getDentalCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=6", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Earnose Catagory 7
    private void getEarNoseCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=7", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Eye Catagory 8
    private void getEyeCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=8", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Sex Catagory 9
    private void getSexCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=9", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Bone Catagory 10
    private void getBoneCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=10", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Diabetes Catagory 11
    private void getDiabetesCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=11", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Cancer Catagory 12
    private void getCancerCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=12", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Kidney Catagory 13
    private void getKidneyCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=13", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Surgery Catagory 14
    private void getSurgeryCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=14", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Brain Catagory 15
    private void getBrainCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=15", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Lungs Catagory 16
    private void getLungsCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=16", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Women Catagory 17
    private void getMentawellnessCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=17", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Dig Catagory 18
    private void getDigestiveCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=18", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Psy Catagory 19
    private void getPhycologistCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=19", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Phy Catagory 20
    private void getPhysiotherapistdCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=20", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Homeo Catagory 21
    private void getHomeopathyCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=21", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }

    //Ayurveda Catagory 22
    private void getAyurvedaCategoryDoctor(final  double longitudeValue, final double latitudeValue) {
        url = String.format("locations?longitude=%f&latitude=%f&category=222", longitudeValue, latitudeValue);

        Call<List<DocterHeartInfo>> call =api.getDocterHeartInfo(url);
        callListDoctorHertInfo(call);
    }



    // Call List Doctor Info
    private void callListDoctorHertInfo(Call<List<DocterHeartInfo>> call) {
        call.enqueue(new Callback<List<DocterHeartInfo>>() {
            @Override
            public void onResponse(Call<List<DocterHeartInfo>> call, Response<List<DocterHeartInfo>> response) {
                if (response.isSuccessful()) {

                    List<DocterHeartInfo> getDoctorSet = response.body();

                    mMap.clear();

                    for (int i = 0; i < getDoctorSet.size(); i++) {
                        latitudeApiValue = getDoctorSet.get(i).getLatitude();
                        longitudeApiValue = getDoctorSet.get(i).getLongitude();
                        doctorName = getDoctorSet.get(i).getName();
                        doctorDegree = getDoctorSet.get(i).getDegrees();
                        //String doctorRating = getDoctorSet.get(i).getName();
                        //String doctorLocation = getDoctorSet.get(i).getName();
                        LatLng latLng = new LatLng(longitudeApiValue, latitudeApiValue);

                        if (mMap!=null){
                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                @Override
                                public View getInfoWindow(Marker marker) {

                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {
                                    View row = getLayoutInflater().inflate(R.layout.custom_info_window,null);
                                    TextView doctorName = row.findViewById(R.id.tvDoctorNameInfoWnidow);
                                    TextView doctorDegree = row.findViewById(R.id.tvDoctorDegreeInfoWnidow);
                                    RatingBar rating = row.findViewById(R.id.rbDoctorRatingInfoWnidow);
                                    TextView doctorLocation = row.findViewById(R.id.tvDoctorLocationInfoWnidow);

                                    doctorName.setText(marker.getTitle());
                                    //doctorDegree.setText(marker.getId());
                                    //doctorLocation.setText(marker.getSnippet());
                                    return row;
                                }
                            });

                            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                @Override
                                public void onInfoWindowClick(Marker marker) {
                                    Intent intent = new Intent(DoctorMapsActivity.this, DoctorProfileActivity.class);
                                    //String title = marker.getTitle();
                                    //intent.putExtra("markertitle", title);
                                    startActivity(intent);
                                }
                            });
                        }

                        mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(CreateCustomMarker(DoctorMapsActivity.this))).title(doctorName));
                        LatLng myLocation = new LatLng(longitudeApiValue, latitudeApiValue);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 7));

                    }
                }
            }

            Bitmap CreateCustomMarker(Context context) {
                return getBitmap(context);
            }

            @Override
            public void onFailure(Call<List<DocterHeartInfo>> call, Throwable t) {
                Toast.makeText(DoctorMapsActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Bitmap Marker
    private Bitmap getBitmap(Context context) {
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        //ImageView markerImage = marker.findViewById(R.id.ivMarker);
        //markerImage.setImageResource(resource);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }



    @Override
    public void onMapReady (GoogleMap googleMap){
        mMap = googleMap;

        LatLng dhaka = new LatLng(23.946556286, 90.84566465);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(dhaka, 8));

        // mMap.addMarker(new MarkerOptions().position(myLocation).title("Marker in Dhaka"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setBuildingsEnabled(true);
        //mMap.setInfoWindowAdapter(new CustomInfoAdapter(DoctorViewMapsActivity.this));
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().isTiltGesturesEnabled();
        mMap.setTrafficEnabled(true);
        mMap.getUiSettings().isCompassEnabled();
        mMap.getUiSettings().isMapToolbarEnabled();
        mMap.stopAnimation();
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }




}
