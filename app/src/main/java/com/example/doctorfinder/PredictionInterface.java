package com.example.doctorfinder;


import com.example.doctorfinder.PlaceAPI.Prediction;

public interface PredictionInterface {
    void getPrediction(Prediction prediction);
}
