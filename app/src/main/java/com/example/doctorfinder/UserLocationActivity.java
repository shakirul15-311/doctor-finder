package com.example.doctorfinder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doctorfinder.PlaceAPI.Prediction;
import com.example.doctorfinder.activitys.MainActivity;
import com.example.doctorfinder.activitys.SetLocationActivity;
import com.example.doctorfinder.databinding.ActivityUserLocationBinding;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserLocationActivity extends AppCompatActivity implements PredictionInterface{
    private PlacesAutoCompleteAdapter placesAutoCompleteAdapter;
    private ActivityUserLocationBinding binding;

    private List<Prediction> predictions;
    private Geocoder geocoder;
    TextView searchMessageTv;

    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;

    List<Address> addresses;

    String longitude, latitude, userLocation;

    SharedPreferences sharedPreferences;

    private final int PICKER_REQUEST_CODE = 1;
    private final int AUTOCOMPLITE_REQUEST_CODE = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_location);

        searchMessageTv = binding.textSearchMessageView;

        sharedPreferences = getSharedPreferences("setLocation", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("isSetLocation",false)){
            Intent intent=new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        initRecyclerView();
        //hideSearchViewIcon();
        searchViewAction();
        searchLocationOnMap();
    }

    private void searchLocationOnMap() {
        Button getLocationBtn = binding.btnSetLocationOnMap;
        getLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showPlacePikerWidget();
                //showAutocompliteView();

                Intent i = new Intent(UserLocationActivity.this,DoctorMapsActivity.class);
                startActivity(i);
            }
        });
    }

    private void showAutocompliteView() {
        AutocompleteFilter filter = new AutocompleteFilter.Builder().setCountry("BD").build();
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(filter).build(this);
            startActivityForResult(intent, AUTOCOMPLITE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void showPlacePikerWidget() {

        try {
            Intent pickerintent = new PlacePicker.IntentBuilder().build(this);
            startActivityForResult(pickerintent, PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }


    }

    private void searchViewAction() {
        binding.searchlocationSV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                predictions.clear();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    predictions.clear();
                    placesAutoCompleteAdapter.notifyDataSetChanged();
                    searchMessageTv.setVisibility(View.VISIBLE);
                }
                else{
                    searchMessageTv.setVisibility(View.GONE);
                    placesAutoCompleteAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });
    }

    private void hideSearchViewIcon() {
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) binding.searchlocationSV.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        magImage.setVisibility(View.GONE);
    }

    private void initRecyclerView() {
        predictions = new ArrayList<>();
        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getApplicationContext(), predictions, this);
        binding.locationNameRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.locationNameRecyclerView.setAdapter(placesAutoCompleteAdapter);
    }

    @Override
    public void getPrediction(Prediction prediction) {
        geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(prediction.getDescription(), 1);
            hideSoftKeyboard();
            if (addresses.size()>0){
                Toast.makeText(this, String.valueOf(addresses.get(0).getLatitude()), Toast.LENGTH_SHORT).show();

                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());
                userLocation = String.valueOf(addresses.get(0).getAdminArea());

                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putBoolean("isSetLocation",true);
                editor.putString("userLocationKey",userLocation);
                editor.putString("longitudeKey",latitude);
                editor.putString("latitudeKey",longitude);
                editor.apply();
                editor.commit();
                Intent intent=new Intent(UserLocationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();



            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case PICKER_REQUEST_CODE :
                if (requestCode == RESULT_OK){
                    Place place = PlacePicker.getPlace(this, data);
                    Toast.makeText(this, ""+place, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Can't select", Toast.LENGTH_SHORT).show();
                }
                break;
            case AUTOCOMPLITE_REQUEST_CODE :
                if (requestCode == RESULT_OK){
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    Toast.makeText(this, ""+place, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Can't select", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
